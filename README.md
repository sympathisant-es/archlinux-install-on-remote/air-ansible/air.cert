Ansible Archlinux Cert
======================

A role to set basic ssl cert config for ssl.
It can generate self certicate or use certbot.

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)


Todo
----
- Be Kind
